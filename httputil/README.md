httputil
====

Common code for dealing with HTTP.

Includes:

* Code for returning JSON responses.

### Documentation

Visit the docs on [gopkgdoc](http://godoc.org/gitte.com/wanttobeamaster/pkg/httputil)

